//
//  InstaPlus.swift
//  Empire
//
//  Created by Sandor ferreira da silva on 20/03/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import Foundation
class InstaPlus {
    var plano: PlanoInstaPlus
    var contaInstagram: [String: String]
    var inspiracoes: String
    var directMarketing: String
    
    init(plano: PlanoInstaPlus, contaInstagram: [String: String], inspiracoes: String, directMarketing: String) {
        self.plano = plano
        self.contaInstagram = contaInstagram
        self.inspiracoes = inspiracoes
        self.directMarketing = directMarketing
    }
}
