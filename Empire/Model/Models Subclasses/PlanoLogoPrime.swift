//
//  PlanoLogoPrime.swift
//  Empire
//
//  Created by Sandor ferreira da silva on 20/03/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import Foundation

enum PlanoLogoPrime {
    case alfa
    case omega
}
