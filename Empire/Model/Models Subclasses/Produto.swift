//
//  Produto.swift
//  Empire
//
//  Created by Sandor ferreira da silva on 20/03/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import Foundation
class Produto {
    var nome: String
    var preco: Double
    
    init(nome: String, preco: Double) {
        self.nome = nome
        self.preco = preco
    }
}
