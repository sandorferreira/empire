//
//  MidiaCerta.swift
//  Empire
//
//  Created by Sandor ferreira da silva on 20/03/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import Foundation

class MidiaCerta {
    var ideia: String
    var textos: String
    var logo: String // trocar por files
    var arteReferencia: String
    var urgente: Bool
    
    init(ideia: String, textos: String, logo: String, arteReferencia: String, urgente: Bool) {
        self.ideia = ideia
        self.textos = textos
        self.logo = logo
        self.arteReferencia = arteReferencia
        self.urgente = urgente
    }

}
