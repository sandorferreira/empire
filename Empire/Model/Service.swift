//
//  Service.swift
//  Empire
//
//  Created by Sandor ferreira da silva on 19/03/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import Foundation

class Service {
    var name: String?
    var description: String?
    
    
    init(name: String?, description: String?) {
        self.name = name
        self.description = description
    }
}
