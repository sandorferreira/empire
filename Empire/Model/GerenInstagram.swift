//
//  GerenInstagram.swift
//  Empire
//
//  Created by Sandor ferreira da silva on 20/03/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import Foundation
class GerenInstagram {
    var contaInstagram: [String: String]
    var ideia: String
    var destaques: String
    var destaqueInformacoes: String
    var logo: FILE
    var arteReferencia: FILE
    var urgente: Bool
    
    init(contaInstagram: [String: String], ideia: String, destaques: String, destaqueInformacoes: String, logo: FILE, arteReferencia: FILE, urgente: Bool) {
        self.contaInstagram = contaInstagram
        self.ideia = ideia
        self.destaques = destaques
        self.destaqueInformacoes = destaqueInformacoes
        self.logo = logo
        self.arteReferencia = arteReferencia
        self.urgente = urgente
    }
}
