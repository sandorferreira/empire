//
//  LogoPrime.swift
//  Empire
//
//  Created by Sandor ferreira da silva on 20/03/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import Foundation
class LogoPrime {
    var plano: PlanoLogoPrime
    var logo: FILE
    var arteReferencia: FILE
    var urgente: Bool
    var briefing: String
    
    init(plano: PlanoLogoPrime, logo: FILE, arteReferencia: FILE, urgente: Bool, briefing: String) {
        self.plano = plano
        self.logo = logo
        self.arteReferencia = arteReferencia
        self.urgente = urgente
        self.briefing = briefing
    }

}
