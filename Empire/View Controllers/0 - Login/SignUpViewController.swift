//
//  SignUpViewController.swift
//  Empire
//
//  Created by Sandor ferreira da silva on 17/03/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase


class SignUpViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var cpfcnpjTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    //confirmation labels
    @IBOutlet weak var confirmPassTF: UITextField!
    @IBOutlet weak var confirmEmailTF: UILabel!
    @IBOutlet weak var confirmCPFLabel: UILabel!
    
    
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var passwordMatchLabel: UILabel!
    
    @IBOutlet weak var bgLightPinkView: UIView!
    @IBOutlet weak var bgDarkPinView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        confirmPassTF.delegate = self
        emailTextField.delegate = self
        passwordTextField.delegate = self
        // Applying design configurations
        
        configureSignupButton()
        configureBackgroundviews()
        activateSignup(false)
    }
    
    
    @IBAction func signUp(_ sender: Any) {
        if checkEmail() && checkPassword() {
            Auth.auth().createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { (user, error) in
                if error == nil {
                    let alert = UIAlertController(title: "Cadastro Realizado!", message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    print(error?.localizedDescription)
                }
            }
        }
    }
    
    // Design Configuration Functions
    
    private func configureSignupButton() {
        signUpButton.layer.cornerRadius = 20.0
    }
    
    private func configureBackgroundviews() {
        bgLightPinkView.transform = CGAffineTransform(rotationAngle: -CGFloat(.pi / (4 * 4.5)))
        bgDarkPinView.transform = CGAffineTransform(rotationAngle: CGFloat((.pi / 4) * 0.07))
    }
    
    private func activateSignup(_ isEnabled: Bool) {
        if isEnabled {
            self.signUpButton.backgroundColor = UIColor.empireBlue
        } else {
            self.signUpButton.backgroundColor = UIColor.empireInactive
        }
    }
    
    @IBAction func dismissSelf(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    

}

extension SignUpViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == confirmPassTF {
            if passwordTextField.text != confirmPassTF.text {
                self.passwordMatchLabel.isHidden = false
                //return false
            } else {
                print("ta igual")
                if emailTextField.text != nil && cpfcnpjTextField.text != nil {
                    activateSignup(true)
                }
            }
        }
    }
    
    func checkPassword() -> Bool {
        if passwordTextField.text != confirmPassTF.text {
            self.passwordMatchLabel.isHidden = false
            return false
        } else {
            self.passwordMatchLabel.isHidden = true
            return true
        }
    }
    
    
    func checkEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: emailTextField.text)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    
    
}
