//
//  ViewController.swift
//  Empire
//
//  Created by Sandor ferreira da silva on 26/02/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController {
    
    // IBOutlets from Storyboard
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var designBackgroundView: UIView!
    @IBOutlet weak var designBackgroundSecondView: UIView!
    
    
    @IBAction func tappedLogin(_ sender: Any) {
        if emailTextField.text == nil || passwordTextField.text == nil {
            let alert = UIAlertController(title: "Ops!", message: "Confira e-mail e senha", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            Auth.auth().signIn(withEmail: emailTextField.text!, password: passwordTextField.text!) { user, error in
                if error == nil {
                    self.performSegue(withIdentifier: "goHome", sender: self)
                } else {
                    print(error?.localizedDescription)
                    let alert = UIAlertController(title: "Ops!", message: "Ocorreu um erro de login, por favor tente novamente.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configuring designables
        configureLoginButton()
        configureBackgroundviews()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if Auth.auth().currentUser != nil {
            self.performSegue(withIdentifier: "goHome", sender: self)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if Auth.auth().currentUser != nil {
            print("usuário")
            self.performSegue(withIdentifier: "goHome", sender: self)
        } else {
            print("nao tem ninguem")
        }
    }
    private func configureLoginButton() {
        loginButton.layer.cornerRadius = 20.0
    }
    
    private func configureBackgroundviews() {
        designBackgroundView.transform = CGAffineTransform(rotationAngle: -CGFloat(.pi / (4 * 4.5)))
        designBackgroundSecondView.transform = CGAffineTransform(rotationAngle: CGFloat((.pi / 4) * 0.07))
    }


}

