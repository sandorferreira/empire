//
//  UIColorExtension.swift
//  Empire
//
//  Created by Sandor ferreira da silva on 17/03/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import UIKit


extension UIColor {
    
    struct Empire {
        static var blue: UIColor {return UIColor(rgb: 0x01043D)}
        static var pink: UIColor {return UIColor(rgb: 0xFE0C97)}
    }
    
    public static var empireBlue: UIColor {
        return UIColor(rgb: 0x01043D)
    }
    
    public static var empirePink: UIColor {
        return UIColor(rgb: 0xFE0C97)
    }
    
    public static var empireInactive: UIColor {
        return UIColor(rgb: 0xD3D3D3)
    }
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}
