//
//  ProductTableViewCell.swift
//  Empire
//
//  Created by Sandor ferreira da silva on 19/03/19.
//  Copyright © 2019 Sandor Ferreira da Silva. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {

    
    @IBOutlet weak var boxBackgroundview: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionText: UITextView!
    
    
    override func awakeFromNib() {
        boxBackgroundview.layer.cornerRadius = 10.0
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        print("selecionou")
        // Configure the view for the selected state
    }

}
